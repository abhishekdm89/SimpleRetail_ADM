package com.retail.core;

public interface Shipping {
	
	double shippingCost(double itemWeight, long upc);
}
