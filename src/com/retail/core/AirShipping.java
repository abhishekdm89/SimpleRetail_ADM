/*
 * Calculate the air shipping cost
 * */


package com.retail.core;

public class AirShipping implements Shipping {
	
	@Override
	public double shippingCost(double itemWeight, long upc) {
		double cost = 0.0;
		long flag = 0;
		upc = upc % 100;
		flag = upc/10;
		cost = 0.58 * flag;
		return cost;
	}
	
}
