/*
 * This class creates batches of various items and stores them into an array list
 * 
 * */
package com.retail.core;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

public class CreateBatch {
	Item item;
	List<Item> itemList;
	ShippingFactory shippingFactory;
	CalculatePrice calculatePrice;
	
	Date date = new Date();
	
	public CreateBatch() {
		itemList = new ArrayList<Item>();
	}
	
	public boolean addItem(long upc, String description, double price, double weight, String shippingMethod, double shippingCost) {
		//for each new item we create a new object and add the respective values
		item = new Item();
		item.setUpc(upc);
		item.setDescription(description);
		item.setPrice(price);
		item.setWeight(weight);
		item.setShippingMethod(shippingMethod);
		item.setShippingCost(shippingCost);
		
		//depending on the shipping method we calculate the shipping cost here
		shippingFactory = new ShippingFactory();
		calculatePrice = new CalculatePrice(item, shippingFactory);
		
		calculatePrice.getTotalPrice();
		
		itemList.add(item);
		
		sortList();
		
		if(item != null) {
			return true;
		}
		return false;
	}
	
	
	//This method would sort all the values in the arraylist by their respective UPC in ascending order
	public void sortList() {
		Collections.sort(itemList);
	}
	
	//This method prints the item details in a particular batch and also outputs the total shipping cost
	public void printBatch() {
		double total = 0.0;
		System.out.println("\n\n");
		System.out.println("***SHIPMENT REPORT***\t\t"+date);
		System.out.println();
		System.out.format("UPC\t\tDescription\t\t\t\tPrice\tWeight\tShip Method\t\tShipping Cost" + "\n");
		System.out.println("----------------------------------------------------------------------------------------------------------------------");
		for(Item item: itemList) {
			System.out.format(item.getUpc()+"\t"+item.getDescription()+"\t"+item.getPrice()+"\t"+item.getWeight()+"\t"+item.getShippingMethod()+"\t\t\t"+String.format("%.2f", item.getShippingCost())+"\n" );
			total = total + item.getShippingCost();
		}
		System.out.println("\n");
		System.out.println("TOTAL SHIPPING COST:\t\t\t"+String.format("%.2f", total));
	}
}
