/*
 * It is a factory class that is responsible for creating instance of Airshipping class 
 * or
 * GroundShipping class depending on the shipment method
 * 
 * */


package com.retail.core;

public class ShippingFactory {
	public Shipping getShipping(String shippingMethod) {
		if(shippingMethod == null) {
			return null;
		}
		
		if(shippingMethod.equalsIgnoreCase("AIR")) {
			return new AirShipping();
		}
		
		if(shippingMethod.equalsIgnoreCase("GROUND")) {
			return new GroundShipping();
		}
		
		return null;
	}
}
