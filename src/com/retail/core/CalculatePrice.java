/*
 * This class is responsible for calculating the shipping cost of each item as per its respective shipping method
 * */


package com.retail.core;

public class CalculatePrice {
	Item item;
	String shippingMethod;
	Shipping shipping;
	ShippingFactory shippingFactory;
	
	public CalculatePrice() {
		
	}
	
	public CalculatePrice(Item item, ShippingFactory shippingFactory) {
		super();
		this.item = item;
		this.shippingFactory = shippingFactory;
		shipping = shippingFactory.getShipping(item.getShippingMethod());
	}
	
	public void getTotalPrice() {
		item.setShippingCost(shipping.shippingCost(item.getWeight(), item.getUpc()));
	}
}
