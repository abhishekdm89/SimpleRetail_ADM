/*
 * Item class represents each items and responsible for storing their respective values
 * 
 * */


package com.retail.core;

import java.util.Comparator;

public class Item implements Comparable<Item>{
	private long upc;
	private String description;
	private double price;
	private double weight;
	private String shippingMethod;
	
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	private double shippingCost;
	
	
	public double getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}
	public long getUpc() {
		return upc;
	}
	public void setUpc(long upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public int compareTo(Item item) {
		if(this.upc > item.upc) {
			return 1;
		}
		else 
			return -1;
	}
	
}
