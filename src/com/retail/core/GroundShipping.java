/*
 * Calculate the ground shipping cost
 * */


package com.retail.core;

public class GroundShipping implements Shipping{
	
	@Override
	public double shippingCost(double weight, long upc) {
		double cost = 0.0;
		cost = weight * 2.5;
		return cost;
	}
}
