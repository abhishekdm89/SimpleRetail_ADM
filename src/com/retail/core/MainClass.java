package com.retail.core;

import java.util.Date;

public class MainClass {
	
	public static void main(String args[]) {
		CreateBatch batch = new CreateBatch();
		boolean flag = false;
		
		flag = batch.addItem(567321101987L, "CD � Pink Floyd, Dark Side Of Moon", 19.99, 0.58, "AIR", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		
		flag = batch.addItem(567321101985L, "CD � Queen, A Night at Opera\t", 20.49, 0.55, "AIR", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		flag = batch.addItem(567321101986L, "CD � Beatles, Abbey Road\t", 17.99, 0.61, "GROUND", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		flag = batch.addItem(567321101984L, "CD � Michael Jackson, Thriller\t", 23.88, 0.5, "GROUND", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		flag = batch.addItem(467321101899L, "iPhone - Waterproof Case\t", 9.75, 0.73, "AIR", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		flag = batch.addItem(467321101878L, "iPhone - Headphones\t\t", 17.25, 3.21, "GROUND", 0.0);
		if(flag) {
			System.out.println("Item added successfully!");
		}
		else
			System.out.println("Unable to add item!");
		
		batch.printBatch();
		
		
		
	
		//System.out.println("***SHIPMENT REPORT***\t\t"+date);
		//System.out.println("UPC\t\tDescription\t\t\t\t\tPrice\t\tWeight\t\tShip Method\tShipping Cost");
		//System.out.println(item.getUpc()+"\t"+item.getDescription()+"\t\t"+item.getPrice()+"\t\t"+item.getWeight()+"\t\t"+item.getShippingMethod()+"\t\t"+item.getShippingCost() );
	}

}
